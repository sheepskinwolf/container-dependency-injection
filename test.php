<?php
include "vendor/autoload.php";
use Uxin603s\Container;
class A{public function __construct(){}}
class B{public function __construct(){}}
class C{public function __construct(){}}
class Test{
	public function __construct(A $a,B $b,C $c){

	}
}
Container::bind("TestA",function(){
	return new A;
});
Container::bind("TestB",function(){
	return new B;
});
Container::bind("TestC",function(){
	return new B;
});
var_dump(Container::get("TestA"));
var_dump(Container::get("TestB"));
var_dump(Container::get("TestC"));
var_dump(Container::get("Test"));
var_dump(new Test(new A,new B,new C));
